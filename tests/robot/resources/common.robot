*** Settings ***
Library           SeleniumLibrary

*** Variables ***
${BROWSER}        Firefox

${LOGIN URL}      http://hotpotato:8000
${SERVERS URL}      http://hotpotato:8000/servers
${PAGES URL}      http://hotpotato:8000/pages
${HANDOVER URL}      http://hotpotato:8000/handover
${MESSAGE URL}      http://hotpotato:8000/message
${ACCOUNT URL}      http://hotpotato:8000/account

${USERNAME test1}     test1@example.com
${PASSWORD test1}     test_password1

${USERNAME test2}     test2@example.com
${PASSWORD test2}     test_password2


*** Keywords ***
#
# Login/logout keywords.
#

Open Browser To Login Page
    Set Selenium Implicit Wait    1 second
    Open Browser    ${LOGIN URL}    ${BROWSER}
    Page Should Contain    Please sign in

Login Page Should Be Open
    Page Should Contain    Please sign in

Input Username
    [Arguments]    ${username}
    Input Text    email    ${username}

Input Password
    [Arguments]    ${password}
    Input Text    password    ${password}

Input Credentials For test1
    Input Username    ${USERNAME test1}
    Input Password    ${PASSWORD test1}

Input Credentials For test2
    Input Username    ${USERNAME test2}
    Input Password    ${PASSWORD test2}

Input Valid Credentials
    Input Credentials For test1

Input Invalid Credentials
    Input Username    blah@blah.blah
    Input Password    blahblahblah

Submit Credentials
    Click Button    submit

Logout
    Click Element       class=dropdown-toggle
    Click Link    Logout

Login
    Login to test1

Login to test1
    Input Credentials For test1
    Submit Credentials

Login to test2
    Input Credentials For test2
    Submit Credentials

#
# Page (webpage) keywords.
#

Page Should Show Pager Name
    Page Should Contain   is currently on pager

#
# Home page keywords.
#

Go To Home Page
    Click Link    Home

Home Page Should Be Open
    Page Should Contain    is currently on pager

#
# Server keywords.
#

Go To Servers Page
    Click Link    Servers

Servers Page Should Be Open
    Page Should Contain    Add a server
    Page Should Contain    Name
    Page Should Contain    Timezone
    Page Should Contain    Last Heartbeat
    Page Should Contain    Alert on Heartbeat
    Page Should Contain    Downtime Heartbeat Alert

#
# Notifications keywords.
#

Go To Notifications Page
    Click Link    Notifications

Notifications Page Should Be Open
    Page Should Contain    Export

#
# Handover keywords.
#

Go To Handover Page
    Click Link    Handover

Handover Page Should Be Open
    Page Should Contain    Use this page to send a pager message saying you have your pager turned on

#
# Send Message keywords.
#

Go To Send Message Page
    Click Link    Send message

Send Message Page Should Be Open
    Page Should Contain    Use this page to send a custom pager message

#
# Account settings keywords.
#

Go To Account Settings Page
    Click Element       class=dropdown-toggle
    Click Link    Account Settings

#
# Change Password keywords.
#

Go To Change Password Page
    Click Element       class=dropdown-toggle
    Click Link    Change Password

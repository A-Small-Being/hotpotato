*** Settings ***
Documentation     Validate the Home page.
Resource          resources/common.robot
Suite Setup       Open Browser To Login Page
Suite Teardown    Close Browser
Test Teardown     Logout


*** Test Cases ***
Open Home Page
    Login
    Home Page Should Be Open
    Page Should Show Pager Name

"""
Hot Potato configuration.
"""


import configparser
import os
import pathlib
from collections import namedtuple

from flask import current_app

FILE_EXT = "ini"


BOOLEAN_STATES = {
    "1": True,
    "yes": True,
    "true": True,
    "on": True,
    "0": False,
    "no": False,
    "false": False,
    "off": False,
}


def _convert_to_boolean(value):
    """
    Return a boolean value translating from other types if necessary.
    """
    if value.lower() not in BOOLEAN_STATES:
        raise ValueError("Not a boolean: %s" % value)
    return BOOLEAN_STATES[value.lower()]


def init_app(app, *filepaths, use_defaults=True, setup_server_name=False):
    """
    Initialise the given Flask app with the Hot Potato configuration.
    """

    file_loaded = False

    # Set up the config parser and associated get methods.
    config = configparser.ConfigParser(strict=True)

    g = namedtuple("g", ["file", "env"])

    gstr = g(config.get, str)
    gbool = g(config.getboolean, _convert_to_boolean)
    gint = g(config.getint, int)

    # Add configuration sections.
    config.add_section("flask")
    config.add_section("hotpotato")
    config.add_section("security")
    config.add_section("mail")
    config.add_section("sqlalchemy")
    config.add_section("rabbitmq")
    config.add_section("modica")
    config.add_section("twilio")
    config.add_section("app")
    config.add_section("sms")
    config.add_section("pager")
    config.add_section("pushover")

    # Try to load default files and filters, if enabled.
    if use_defaults:
        res = _add_path(config, "/etc/hotpotato/conf.d", default=True)
        if res and not file_loaded:
            file_loaded = True

        if not file_loaded:
            _add_path(config, "config.{}".format(FILE_EXT), default=True)
            if res and not file_loaded:
                file_loaded = True

    # Load specified configuration files.
    for filepath in filepaths:
        if _add_path(config, filepath, required=True) and not file_loaded:
            file_loaded = True

    # Read configuration parameters from the ConfigParser object and environment into the app.
    app.config.update(
        [
            _get(gbool, "flask", "testing", default=False),
            _get(gstr, "flask", "secret_key"),
            _get(gstr, "hotpotato", "webui_url"),
            _get(gstr, "hotpotato", "troublecode_url"),
            _get(gstr, "hotpotato", "log_level", default="WARNING"),
            _get(gstr, "security", "password_hash", default="bcrypt"),
            _get(gstr, "security", "password_salt"),
            _get(gbool, "security", "trackable", default=True),
            _get(gbool, "security", "changeable", default=True),
            _get(gbool, "security", "confirmable", default=False),
            _get(gbool, "security", "recoverable", default=True),
            _get(gbool, "mail", "suppress_send", default=False),
            _get(gstr, "mail", "default_sender", default=None),
            _get(gstr, "mail", "server", default=None),
            _get(gint, "mail", "port", default=587),
            _get(gbool, "mail", "use_ssl", default=True),
            _get(gstr, "mail", "username", default=None),
            _get(gstr, "mail", "password", default=None),
            _get(gstr, "cockroach", "server"),
            _get(gint, "cockroach", "port"),
            _get(gstr, "cockroach", "database"),
            _get(gstr, "cockroach", "username"),
            _get(gstr, "cockroach", "password"),
            _get(gstr, "rabbitmq", "server"),
            _get(gint, "rabbitmq", "port", default=5672),
            _get(gbool, "rabbitmq", "use_ssl", default=False),
            _get(gstr, "rabbitmq", "username"),
            _get(gstr, "rabbitmq", "password"),
            _get(gstr, "rabbitmq", "vhost", default="/"),
            _get(gstr, "modica", "enabled", default=True),
            _get(gstr, "twilio", "enabled", default=True),
            _get(gbool, "app", "enabled", default=False),
            _get(gbool, "pager", "enabled", default=True),
            _get(gbool, "sms", "enabled", default=True),
            _get(gbool, "pushover", "enabled", default=True),
        ]
    )

    app.logger.setLevel(app.config["HOTPOTATO_LOG_LEVEL"])

    if app.config["COCKROACH_USERNAME"]:
        app.config["SQLALCHEMY_DATABASE_URI"] = "cockroachdb://{}:{}@{}:{}/{}".format(
            app.config["COCKROACH_USERNAME"],
            app.config["COCKROACH_PASSWORD"],
            app.config["COCKROACH_SERVER"],
            app.config["COCKROACH_PORT"],
            app.config["COCKROACH_DATABASE"],
        )
    else:
        app.config["SQLALCHEMY_DATABASE_URI"] = "cockroachdb://{}:{}/{}".format(
            app.config["COCKROACH_SERVER"],
            app.config["COCKROACH_PORT"],
            app.config["COCKROACH_DATABASE"],
        )

    default_url_scheme = "http" if app.config["TESTING"] else "https"

    app.config.update(
        [_get(gstr, "flask", "preferred_url_scheme", default=default_url_scheme)]
    )

    if app.config["PUSHOVER_ENABLED"]:
        app.config.update(
            [
                _get(gstr, "pushover", "api_token"),
                _get(gint, "pushover", "alert_priority", default=2),
                _get(gint, "pushover", "message_priority", default=0),
                _get(gint, "pushover", "handover_priority", default=0),
                # How long and ofter to retry for emergency priority 2 alerts
                # default to every 2 minutes for 3 hours
                _get(gint, "pushover", "emergency_retry", default=120),
                _get(gint, "pushover", "emergency_expiry", default=10800),
            ]
        )

    if app.config["MODICA_ENABLED"]:
        app.config.update(
            [
                _get(gstr, "modica", "url"),
                _get(gstr, "modica", "username"),
                _get(gstr, "modica", "password"),
            ]
        )

    if app.config["TWILIO_ENABLED"]:
        app.config.update(
            [
                _get(gstr, "twilio", "account_sid"),
                _get(gstr, "twilio", "auth_token"),
                _get(gstr, "twilio", "sms_number"),
            ]
        )

    if setup_server_name:
        app.config["SERVER_NAME"] = app.config["HOTPOTATO_WEBUI_URL"]


def _add_path(config, filepath, required=False, default=False, recurse=False):
    """
    Load configuration from the given filepath into the given ConfigParser object.
    """

    # pylint: disable=too-many-locals

    file_loaded = False

    path = pathlib.Path(filepath)

    if path.is_dir():
        for subpath in path.iterdir():
            if (subpath.is_dir() and recurse) or subpath.is_file:
                res = _add_path(
                    config, str(subpath), required=required, recurse=recurse
                )
                file_loaded = True if res else path

    else:
        try:
            with path.open("r") as fil:
                cpr = configparser.ConfigParser(strict=True)
                cpr.read_file(fil, source=str(path))
                for section_name, section in cpr.items():
                    for key, value in section.items():
                        config.set(section_name.lower(), key, value)
                file_loaded = True

        except IOError as err:
            if required:
                raise IOError(
                    "Unable to read required configuration file {}".format(path)
                ) from err
            elif not default:
                current_app.logger.warning(
                    "Unable to read configuration file {}: {}".format(
                        path, err.strerror
                    )
                )

    return file_loaded


def _get(gtype, section, key, default=None):
    """
    Get a configuration key
    """
    name = (
        "{}_{}".format(section.upper(), key.upper())
        if section != "flask"
        else key.upper()
    )
    env_name = "HOTPOTATO_{}_{}".format(section.upper(), key.upper())
    if env_name in os.environ:
        return (name, gtype.env(os.environ.get(env_name)))
    return (name, gtype.file(section, key, fallback=default))

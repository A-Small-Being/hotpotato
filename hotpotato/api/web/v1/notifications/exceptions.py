"""
Web API version 1 notification exception classes.
"""


from hotpotato.api.web.v1 import exceptions as api_web_v1_exceptions


class APIWebV1NotificationError(api_web_v1_exceptions.APIWebV1Error):
    """
    Web API version 1 notification exception base class.
    """

    pass


class APIWebV1NotificationHandlerError(APIWebV1NotificationError):
    """
    Web API version 1 notification handler exception.
    """

    pass

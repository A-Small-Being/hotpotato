from http import HTTPStatus

from flask import current_app
from werkzeug.exceptions import HTTPException

from hotpotato.api import exceptions as api_exceptions
from hotpotato.api.web.v1 import functions
from hotpotato.api.web.v1._blueprint import blueprint


@blueprint.errorhandler(api_exceptions.APIError)
def handle_api_error(err):
    current_app.logger.exception(err)
    return functions.api_json_response_get(
        success=err.success, code=err.code, message=str(err)
    )


@blueprint.errorhandler(HTTPException)
def handle_http_exception(err):
    return functions.api_json_response_get(
        success=False, code=err.code, message=err.description
    )


@blueprint.errorhandler(Exception)
def handle_exception(err):
    current_app.logger.exception(err)
    return functions.api_json_response_get(
        success=False, code=HTTPStatus.INTERNAL_SERVER_ERROR, message=str(err)
    )

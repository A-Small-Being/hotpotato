"""
Server API endpoints for Modica callbacks.
"""


import flask
from flask import current_app
from webargs import fields
from webargs.flaskparser import parser

from hotpotato import models, oncall_contacts
from hotpotato.notifications import (
    exceptions as notifications_exceptions,
    messages,
    notifications,
)

mo_args = {
    "id": fields.Int(required=True),
    "source": fields.Str(required=True),
    "destination": fields.Str(required=True),
    "content": fields.Str(required=True),
    "operator": fields.Str(required=True),
    "reply-to": fields.Str(required=True),
}


def mo_message():
    """
    Handle incoming SMS messages from Modica.
    """

    json_obj = parser.parse(mo_args, flask.request, locations=("json",))

    # Here just in case we actually need it for something.
    # provider_notif_id = json_obj["id"]

    contact = json_obj["source"]
    operator = json_obj["operator"]
    reply_to_provider_notif_id = json_obj["reply-to"]
    body = json_obj["content"]

    current_app.logger.debug("Receiving message from Modica:")
    current_app.logger.debug("- contact = {}".format(contact))
    current_app.logger.debug("- operator = {}".format(operator))
    current_app.logger.debug(
        "- reply_to_provider_notif_id = {}".format(reply_to_provider_notif_id)
    )
    current_app.logger.debug("- body = {}".format(body))

    # Get the original notification being responded to.
    try:
        reply_to_notif = notifications.get_by_provider_notif_id(
            "modica", reply_to_provider_notif_id
        )
    except notifications_exceptions.NotificationProviderNotificationIDError as err:
        err_message = "While receiving incoming message from Modica: {}".format(
            str(err)
        )
        current_app.logger.error(err_message)
        return ("", 400)

    # If this message was a response to another message or handover, get the sender for
    # the original message and make them the recipient for this message.
    if (
        reply_to_notif.notif_type == "message"
        or reply_to_notif.notif_type == "handover"
    ) and reply_to_notif.json["from_user_id"]:
        to_user_id = reply_to_notif.json["from_user_id"]
    else:
        to_user_id = None

    # Get the user ID who sent the message from their SMS contact number.
    # TODO: Issue #177 / Merge Request !145 - Check if this needs to support pager somehow.
    from_user_id = next(oncall_contacts.get_by(method="sms", contact=contact)).user_id

    current_app.logger.debug(
        "Setting status of original notification "
        "with ID {} to READ_BY_CLIENT".format(reply_to_notif.id)
    )
    reply_to_notif.json["status"] = "READ_BY_CLIENT"
    models.db.session.commit()

    mess = messages.create(
        tenant_id=reply_to_notif.tenant_id,
        body=body,
        to_user_id=to_user_id,
        from_user_id=from_user_id,
        reply_to_notif_id=reply_to_notif.id,
    )
    if to_user_id:
        mess.send()

    return ("", 204)


dlr_args = {
    "message_id": fields.Str(required=True),
    "status": fields.Str(required=True),
}


def notificationstatus():
    """
    Handle delivery status receipts from Modica.
    """

    json_obj = parser.parse(dlr_args, flask.request, locations=("json",))

    provider_notif_id = json_obj["message_id"]
    provider_notif_status = json_obj["status"]

    current_app.logger.debug("Received delivery receipt from Modica:")
    current_app.logger.debug("- provider_notif_id = {}".format(provider_notif_id))
    current_app.logger.debug(
        "- provider_notif_status = {}".format(provider_notif_status)
    )

    # Try to find the notification associated with the provider notification ID.
    try:
        notif = notifications.get_by_provider_notif_id("modica", provider_notif_id)
    except notifications_exceptions.NotificationProviderNotificationIDError as err:
        err_message = "While receiving message status from Modica: {}".format(str(err))
        current_app.logger.error(err_message)
        return ("", 400)

    current_app.logger.debug(
        "Found associated notification with ID: {}".format(notif.id)
    )

    # Modify the status of the message based on the information in the delivery receipt.
    if provider_notif_status == "submitted":
        notif.json["status"] = "RECEIVED_BY_PROVIDER"
    elif provider_notif_status == "sent":
        notif.json["status"] = "SENT_TO_CLIENT"
    elif provider_notif_status == "received":
        notif.json["status"] = "RECEIVED_BY_CLIENT"
    else:
        notif.json["status"] = "SEND_FAILED"
        if provider_notif_status == "frozen":
            notif.json["errors"].append(
                "Modica: A transient error has frozen this message"
            )
        elif provider_notif_status == "rejected":
            notif.json["errors"].append(
                "Modica: The carrier rejected the message "
                "(perhaps check validity of the contact number)"
            )
        elif provider_notif_status == "failed":
            notif.json["errors"].append(
                "Modica: Message delivery has failed due to "
                "carrier connectivity issue"
            )
        elif provider_notif_status == "dead":
            notif.json["errors"].append("Modica: Message killed by administrator")
        elif provider_notif_status == "expired":
            notif.json["errors"].append(
                "Modica: The carrier was unable to deliver the message "
                "in the specified amount of time"
            )
        else:
            notif.json["errors"].append(
                "Received unknown DLR status from Modica: {}".format(
                    provider_notif_status
                )
            )

    current_app.logger.debug(
        "Changed status of notification with ID {} to {}".format(
            notif.id, notif.json["status"]
        )
    )

    if notif.json["status"] == "SEND_FAILED":
        current_app.logger.error(
            "Failed to send notification with ID {}: {}".format(
                notif.id, notif.json["errors"][-1]
            )
        )

    models.db.session.commit()

    return ("", 204)

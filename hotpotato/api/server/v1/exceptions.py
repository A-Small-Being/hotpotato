"""
Server API version 1 exception classes.
"""


from hotpotato.api import exceptions as api_exceptions


class APIServerV1Error(api_exceptions.APIError):
    """
    Server API version 1 exception base class.
    """

    pass


class APIServerV1HandlerError(APIServerV1Error):
    """
    Server API version 1 notification handler exception.
    """

    pass


class APIServerV1AuthError(APIServerV1Error):
    """
    Server API version 1 authentication exception.
    """

    pass


class APIServerV1ResponseError(APIServerV1Error):
    """
    Server API version 1 response exception.
    """

    pass

"""
Server API version 1 blueprint.
"""


import flask

blueprint = flask.Blueprint("api_server_v1", __name__)

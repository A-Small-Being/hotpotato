"""
Server uptime statistics unit test functions.
"""


from hotpotato import models


def create_fake(fake, server):
    """
    Generate a fake server uptimes statistics entry for the given server,
    with randomly set data.
    """

    return models.ServerUptime(
        server_id=server.id,
        updated_dt=fake.past_datetime(start_date="-30d"),
        avail_last_1d=fake.random.triangular(0, 1, 100),
        avail_last_7d=fake.random.triangular(0, 1, 100),
        avail_last_month=fake.random.triangular(0, 1, 100),
    )

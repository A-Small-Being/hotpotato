"""
Test the pushover callback endpoint.
"""

from http import HTTPStatus

from hotpotato.tests import notifications, users


def test_pushover_notification_status_no_body(client, session):
    """
    Test that a notification update with no body returns 422.
    """
    r = client.post("/api/server/v2/senders/pushover/notificationstatus")
    assert r.status_code == HTTPStatus.UNPROCESSABLE_ENTITY


def test_pushover_notification_status_invalid_id(client, session):
    """
    Test that a notification update for a notification that does not exist returns 400.
    """
    r = client.post(
        "/api/server/v2/senders/pushover/notificationstatus",
        data={
            "receipt": "5",
            "acknowledged": "1",
            "acknowledged_at": "1360019238",
            "acknowledged_by": "dfjh83",
            "acknowledged_by_device": "Test Phone",
        },
    )
    assert r.status_code == HTTPStatus.BAD_REQUEST


def test_pushover_notification_status_success(client, session):
    """
    Test that a valid notification update, updates the notification status successully and returns 204.
    """
    users.UserFactory()
    session.commit()
    alert = notifications.NotificationFactory(notif_type="alert")
    session.commit()

    alert.json["provider"] = "pushover"
    alert.json["provider_notif_id"] = "5"
    session.add(alert)
    session.commit()
    r = client.post(
        "/api/server/v2/senders/pushover/notificationstatus",
        data={
            "receipt": "5",
            "acknowledged": "1",
            "acknowledged_at": "1360019238",
            "acknowledged_by": "dfjh83",
            "acknowledged_by_device": "Test Phone",
        },
    )
    assert r.status_code == HTTPStatus.NO_CONTENT

"""
Test the V1 heartbeat endpoint
"""

from http import HTTPStatus

from hotpotato import heartbeats
from hotpotato.tests import servers


def test_heartbeat_no_apikey(client, session):
    """
    Test that not providing an apikey returns 400.
    """
    r = client.post("/api/server/v1/heartbeat")
    assert r.status_code == HTTPStatus.BAD_REQUEST


def test_heartbeat_invalid_server(client, session):
    """
    Test that providing an invalid apikey returns a 401.
    """
    r = client.post("/api/server/v1/heartbeat", data=dict(apikey="notavalidapikey"))
    assert r.status_code == HTTPStatus.UNAUTHORIZED


def test_heartbeat_success(client, session):
    """
    Test that providing a valid apikey successfully creates a heartbeat.
    """
    server = servers.ServerFactory()
    session.commit()

    r = client.post("/api/server/v1/heartbeat", data=dict(apikey=server.apikey))
    assert r.status_code == HTTPStatus.OK

    heartbeat = heartbeats.get_last(server_id=server.id)
    assert heartbeat is not None

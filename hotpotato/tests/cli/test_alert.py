from hotpotato import rotations
from hotpotato.notifications import messages
from hotpotato.tests import notifications, oncall_contacts, servers


def test_check_failed_0(runner, session):
    """
    Run check-failed on an empty database and check that it exits without errors.
    """
    result = runner.invoke(args=["alert", "check-failed"])
    assert " 0 failed alerts" in result.output
    assert result.exit_code is 0


def test_check_failed(runner, session, make_user, app):
    """
    Run check-failed on a database with a failed alaert and check that it sends a message.
    """
    oncall_user = make_user("admin")
    rotations.create("sysadmins", oncall_user.id)
    servers.ServerFactory()
    oncall_contacts.OncallContactsFactory(user=oncall_user)
    alert = notifications.NotificationFactory(notif_type="alert")
    alert.json["status"] = "SEND_FAILED"
    session.commit()

    count = len(messages.get_by())
    result = runner.invoke(args=["alert", "check-failed"])
    assert " 1 failed alerts" in result.output
    assert result.exit_code is 0
    assert len(messages.get_by()) > count

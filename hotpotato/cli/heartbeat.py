"""
Command line interface (CLI) heartbeat functions.
"""


import click
import flask.cli

from hotpotato.heartbeats import delete_old as _delete_old


@click.group("heartbeat", cls=flask.cli.AppGroup)
def heartbeat():
    """
    Heartbeat commands.
    """

    pass


@heartbeat.command("delete-old")
def delete_old():
    """
    Purge the heartbeats table of old heartbeats.
    """

    _delete_old()

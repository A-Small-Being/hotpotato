"""
Command line interface (CLI) alert functions.
"""


import click
import flask.cli
from flask import current_app

from hotpotato import oncall_contacts, proxies, util
from hotpotato.notifications import alerts, messages


@click.group("alert", cls=flask.cli.AppGroup)
def alert():
    """
    Alert commands.
    """

    pass


@alert.command("get")
@click.argument("ale_id", type=click.INT)
def get(ale_id):
    """
    Get an alert.
    """

    click.echo(alerts.get(ale_id).as_json(indent=2))


@alert.command("check-failed")
def check_failed():
    """
    Run the check for failed alerts.
    """

    tenant_id = None  # TODO: for tenant in tenants:
    current_app.config["SERVER_NAME"] = current_app.config["HOTPOTATO_WEBUI_URL"]

    num_failed_ales = alerts.get_num_failed()

    click.echo("There are {} failed alerts".format(num_failed_ales))

    if num_failed_ales:
        body = (
            "{}: TATER001 Intervention required, "
            "there are {} alerts that failed to page"
        ).format(util.node_name, num_failed_ales)

        click.echo(body)

        # Send a message to the on-call person.
        # TODO: custom rotations
        oncall_user = proxies.get_current_pager_person(rotation="sysadmins")

        click.echo("Paging on-call person {}".format(oncall_user.name))
        messages.create(tenant_id=tenant_id, body=body, to_user_id=oncall_user.id).send(
            run_async=False
        )

        # Notify all the people that wanted to know.
        for contact in oncall_contacts.get_by(send_failures=True):
            click.echo(
                "Notifying {} of the failure via {}".format(
                    contact.contact, contact.method
                )
            )
            messages.create(
                tenant_id=tenant_id, body=body, to_user_id=contact.user_id
            ).send(contact=contact)

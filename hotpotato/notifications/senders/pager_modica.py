"""
Notification Modica Pager sender classes and functions.
"""

import flask

from hotpotato.notifications.senders.modica import ModicaBaseSender
from hotpotato.notifications.senders.pager import PagerBaseSender


class ModicaPagerSender(ModicaBaseSender, PagerBaseSender):
    """
    Notification Modica Pager sender.
    """

    name = "pager_modica"
    description = "Modica Pager"

    def can_send(self, user_id):
        """
        Return True if the user with the given ID can use the pager sender,
        and sending via pager is enabled and Modica is enabled.
        """
        return (
            super().can_send(user_id)
            if flask.current_app.config["MODICA_ENABLED"]
            else False
        )

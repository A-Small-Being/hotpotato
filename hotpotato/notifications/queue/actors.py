"""
Notification message queue actor functions.
"""


import dramatiq
import flask

# When Hot Potato itself addresses the actors, there is a Flask app context available,
# but when this file is started as a Dramatiq worker, there is none.
# This creates a basic one that allows the Dramatiq workers to connect to the message queue,
# the database and everything else needed to send notifications.
if flask.has_app_context():
    from flask import current_app as app
else:
    from hotpotato.app.worker import app

PRIORITY_HANDOVER = 0
PRIORITY_ALERT = 10
PRIORITY_MESSAGE = 20


@dramatiq.actor(priority=PRIORITY_ALERT, max_retries=3)
def alert(notif_id, contact_id=None, method=None):
    """
    Send the given alert, with the appropriate priority.
    """

    with app.app_context():
        from hotpotato.notifications import alerts

        actor_send(alerts.get(notif_id), contact_id=contact_id, method=method)


@dramatiq.actor(priority=PRIORITY_HANDOVER, max_retries=3)
def handover(notif_id, contact_id=None, method=None):
    """
    Send the given handover notification, with the appropriate priority.
    """

    with app.app_context():
        from hotpotato.notifications import handovers

        actor_send(handovers.get(notif_id), contact_id=contact_id, method=method)


@dramatiq.actor(priority=PRIORITY_MESSAGE, max_retries=3)
def message(notif_id, contact_id=None, method=None):
    """
    Send the given message, with the appropriate priority.
    """

    with app.app_context():
        from hotpotato.notifications import messages

        actor_send(messages.get(notif_id), contact_id=contact_id, method=method)


def actor_send(notif, contact_id=None, method=None):
    """
    Helper method to perform the sending action.
    """

    from hotpotato.notifications import send

    if contact_id:
        send.using_contact_id(notif, contact_id)
    elif method:
        send.using_method(notif, method)
    else:
        send.using_all(notif)

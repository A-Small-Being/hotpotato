"""
Sender functions.
"""


import types

from flask import current_app

from hotpotato import notifications, oncall_contacts
from hotpotato.models import db
from hotpotato.notifications import exceptions, senders


def using_all(notif):
    """
    Try to send a notification using all available senders, in Hot Potato's preferred order.
    """

    _send(notif, oncall_contacts.get_for_user_id(notif.user_id))


def using_contact_id(notif, contact_id):
    """
    Try to send a notification using a given contact ID.
    """

    try:
        contact = oncall_contacts.get(contact_id)
    except oncall_contacts.OncallContactIDError as err:
        raise exceptions.NotificationSendError(
            "While trying to send {} with ID {} "
            "to contact with ID {}: {}".format(
                notif.description, notif.id, contact_id, str(err)
            )
        )

    using_contact(notif, contact)


def using_contact(notif, contact):
    """
    Try to send a notification using a given contact.
    """

    _send(notif, tuple((contact,)))


def using_method(notif, method):
    """
    Try to send a notification using a given sending method.
    """

    if method not in notifications.METHOD:
        raise exceptions.NotificationSendError(
            "Invalid or unsupported sending method '{}'".format(method)
        )

    _send(notif, oncall_contacts.get_by(user_id=notif.user_id, method=method))


def using_sender(notif, sender, contact):
    """
    Try to send a notification using a given sender and a given contact.
    Usually only used for testing purposes.
    """

    _send(
        notif,
        tuple((contact,)),
        types.MappingProxyType({sender.method: tuple((sender,))}),
    )


# pylint: disable=too-many-statements,too-many-branches,too-many-locals
def _send(notif, contacts, senders_for_method=None):
    """
    Sending helper method that does all the heavy lifting.
    """

    # TODO: add support for tenants?

    if not senders_for_method:
        senders_for_method = senders.FOR_METHOD

    # Data to update the notification with.
    success = False
    tried_to_send = False
    errors = []
    warnings = []
    method = None
    provider = None
    provider_notif_id = None

    # Sanity checking.
    if notif.is_sending():
        return
    if not notif.user_id:
        notif.json["status"] = "SEND_FAILED"
        notif.json["errors"].append(
            "While sending: Notification with ID {} "
            "has no destination user ID".format(notif.id)
        )
        return

    # Set the notification status to SENDING as early as possible in the process,
    # to put a lock on the notification log entry so that other workers do not try
    # to send this notification, if it is somehow on the queue more than once.
    notif.json["status"] = "SENDING"
    db.session.commit()

    # Catch all exceptions from this point and make sure they get logged into the notification.
    # pylint: disable=too-many-nested-blocks
    try:
        for i, contact in enumerate(contacts):
            # When sending an alert we need to check that the user has a contact method
            # where sending pages has been enabled
            if notif.notif_type == "alert" and not contact.send_pages:
                current_app.logger.debug(
                    "Skipped sending because send_pages is disabled "
                    "for contact with ID {} for user {}".format(
                        contact.id, notif.user_id
                    )
                )
                continue
            tried_to_send = True
            data = None

            try:
                # TODO: finish this part
                for sender in senders_for_method[contact.method]:

                    current_app.logger.debug(
                        "Attempting to send {} using {}".format(
                            notif.description, sender.description
                        )
                    )

                    data = sender.send(notif, contact)

                    errors.extend(data["errors"])
                    warnings.extend(data["warnings"])

                    if data["success"]:
                        success = True
                        method = sender.method
                        provider = sender.provider
                        provider_notif_id = data["provider_notif_id"]
                        break  # Operation succeeded, breaks out here to update the object.

                    else:
                        err_message = "Failed to send using {}".format(
                            sender.description
                        )
                        errors.append(err_message)
                        current_app.logger.error(err_message)
                        if (i + 1) < len(contacts):
                            current_app.logger.error(
                                "Will now try to send using {} with contact number '{}'".format(
                                    senders_for_method[
                                        contacts[i + 1].method
                                    ].description,
                                    contacts[i + 1].contact,
                                )
                            )

            # pylint: disable=broad-except
            except Exception as err:
                err_message = "Failed to send using {}: {}".format(
                    sender.description, str(err)
                )
                errors.append(err_message)
                current_app.logger.exception(err_message)
                if (i + 1) < len(contacts):
                    current_app.logger.error(
                        "Will now try to send using {} with contact number '{}'".format(
                            senders_for_method[contacts[i + 1].method].description,
                            contacts[i + 1].contact,
                        )
                    )

        # Update the notification object with the sending status.
        notif.json["status"] = "SENT_TO_PROVIDER" if success else "SEND_FAILED"
        notif.json["errors"] = errors
        notif.json["warnings"] = warnings
        notif.json["method"] = method
        notif.json["provider"] = provider
        notif.json["provider_notif_id"] = provider_notif_id
        db.session.commit()

    # pylint: disable=broad-except
    except Exception as err:
        err_message = "While sending: {}".format(str(err))
        errors.append(err_message)
        current_app.logger.exception(err_message)

        notif.json["status"] = "SEND_FAILED"
        notif.json["errors"] = errors
        notif.json["warnings"] = warnings
        notif.json["method"] = method
        notif.json["provider"] = provider
        notif.json["provider_notif_id"] = str(
            provider_notif_id
        )  # Double check it's a string.
        db.session.commit()

    if not tried_to_send:
        output = (
            "Unable to find a sending provider that could send the {} "
            "to user with ID {}\n"
            "Please check that the user has on-call contacts configured"
        ).format(notif.description, notif.user_id)
        raise exceptions.NotificationSendError(output)

    # Handle error output if sending failed.
    if not success:
        output = "Failed to send {} with ID {}\n".format(notif.description, notif.id)
        if warnings:
            output += "\nWarnings:\n"
            for warning in warnings:
                output += "* {}\n".format(warning)
            output += "\n"
        if errors:
            output += "\nErrors:\n"
            for error in errors:
                output += "* {}\n".format(error)
            output += "\n"
        output += "Please check the {}'s log entry for more details".format(
            notif.description
        )
        raise exceptions.NotificationSendError(output)

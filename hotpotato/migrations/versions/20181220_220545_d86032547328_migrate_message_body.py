"""
Migrate message body

Revision ID: d86032547328
Revises: 7fe1a193b6ce
Create Date: 2018-12-20 22:05:45.136990
"""


import sqlalchemy as sa
from alembic import context, op

# revision identifiers, used by Alembic.
revision = "d86032547328"
down_revision = "7fe1a193b6ce"
branch_labels = None
depends_on = None

num_insert_ops = 1


def database_insert(table, data, row=None):
    """
    Helper function for adding data to a table.

    Give it the table and a mutable data list, and it will insert rows in a controlled
    manner, by splitting the insert operation into smaller ones with a maximum 100 rows.

    Insert data into it by passing it to the row keyword argument, as a dictionary.

    Once all the data has been run through this function, call it one more time
    without the row parameter to flush the rest of tha data.
    """

    global num_insert_ops

    if not row or len(data) >= 100:
        print("{} INSERT {}".format(num_insert_ops, len(data)))
        op.bulk_insert(table, data)
        data.clear()
        num_insert_ops += 1

    if row:
        data.append(row)


def row_select(session, table, *columns, **search_params):
    """
    Return the results of a SELECT query.

    Returns a generator of single elements if there was only one column specified,
    otherwise returns a generator of arrays of elements.
    """

    cols = [table.columns[col] for col in columns]

    query = session.query(*cols)

    if search_params:
        for key, value in search_params.items():
            query = query.filter(table.columns[key] == value)

    return (data[0] for data in query) if len(columns) == 1 else query


def row_update(table, row_id, **columns):
    """
    Update a row on the given table with the given column values.
    """

    context.execute(table.update().where(table.c.id == row_id).values(**columns))


def upgrade():
    """
    Upgrade the database.
    """

    print("Performing upgrade migration...")

    with context.begin_transaction():
        schema_upgrade()
    data_upgrade()

    print("Upgrade migration complete.")


def downgrade():
    """
    Downgrade the database to the previous version.
    """

    print("Performing downgrade migration...")

    data_downgrade()
    with context.begin_transaction():
        schema_downgrade()

    print("Downgrade migration complete.")


def schema_upgrade():
    """
    Upgrade the database schema.
    """

    print("Performing schema upgrade...")

    # NOTE: Add print statements for each statement.
    # print("Adding column 'column_name'...")
    pass

    print("Schema upgrade complete.")


def schema_downgrade():
    """
    Downgrade the database schema to the previous version.
    """

    print("Performing schema downgrade...")

    # NOTE: Add print statements for each statement.
    # print("Dropping column 'column_name'...")
    pass

    print("Schema downgrade complete.")


def data_upgrade():
    """
    Upgrade the database data.
    """

    with context.begin_transaction():
        bind = op.get_bind()
        session = sa.orm.Session(bind=bind)

        user = sa.Table(
            "user",
            sa.MetaData(bind=bind),
            sa.Column("id", sa.Integer, primary_key=True),
            sa.Column("name", sa.Text),
        )

        notif_types = frozenset(("alert", "handover", "message"))

        notification_log = sa.Table(
            "notification_log",
            sa.MetaData(bind=bind),
            sa.Column("id", sa.Integer, primary_key=True),
            sa.Column("tenant_id", sa.Integer),
            sa.Column(
                "user_id", sa.Integer, sa.ForeignKey("User.id", ondelete="SET NULL")
            ),
            sa.Column("received_dt", sa.DateTime, nullable=False),
            sa.Column("notif_type", sa.Enum(*notif_types), nullable=False),
            sa.Column("body", sa.Text, nullable=False),
            sa.Column(
                "json", sa.ext.mutable.MutableDict.as_mutable(sa.JSON), nullable=False
            ),
        )

        messages = row_select(
            session, notification_log, "id", "body", "json", notif_type="message"
        )

        for message in messages:
            message_body = message[1]
            json = message[2]
            json["message"] = message_body
            body = (
                "Message from {} via Hot Potato: {}".format(
                    row_select(session, user, "name", id=json["from_user_id"]),
                    message_body,
                )
                if json["from_user_id"]
                else "Message from Hot Potato: {}".format(message_body),
            )

            row_update(notification_log, message[0], body=body, json=json)


def data_downgrade():
    """
    Downgrade the database data to the previous version.
    """

    with context.begin_transaction():
        bind = op.get_bind()
        session = sa.orm.Session(bind=bind)

        notif_types = frozenset(("alert", "handover", "message"))

        notification_log = sa.Table(
            "notification_log",
            sa.MetaData(bind=bind),
            sa.Column("id", sa.Integer, primary_key=True),
            sa.Column("tenant_id", sa.Integer),
            sa.Column(
                "user_id", sa.Integer, sa.ForeignKey("User.id", ondelete="SET NULL")
            ),
            sa.Column("received_dt", sa.DateTime, nullable=False),
            sa.Column("notif_type", sa.Enum(*notif_types), nullable=False),
            sa.Column("body", sa.Text, nullable=False),
            sa.Column(
                "json", sa.ext.mutable.MutableDict.as_mutable(sa.JSON), nullable=False
            ),
        )

        messages = row_select(
            session, notification_log, "id", "json", notif_type="message"
        )

        for message in messages:
            json = message[1]
            body = json["message"]
            del json["message"]

            row_update(notification_log, message[0], body=body, json=json)
